#!/bin/bash

type(){
	TYPE=$1
	echo $TYPE
	if [ $TYPE == "VANILLA" ]
	then
        	echo "You have choosen 'Vanilla'"
		return
	fi
	if [ $TYPE == "SPIGOT" ]
	then
        	echo "You have choosen 'Spigot'"
		return 
	fi
	echo "Error: wrong type"
	exit 1
}

version(){
	VERSION=$1
	if [ $VERSION == "1.8.9" ]
	then
        	echo "You have choosen '1.8.9'"
		return 
	fi
	if [ $VERSION == "1.14.4" ]
	then
        	echo "You have choosen '1.14.4'"
		return
	fi
	if [ $VERSION == "1.16.1" ]
	then
       		echo "You have choosen '1.16.1'"
		return
	fi
	if [ $VERSION == "1.19.3" ]
	then
        	echo "You have choosen '1.19.3'"
		return
	fi
	echo "Error: wrong version"
	exit 1
}

echo "#Welcome to MCServerSetup#"

#####_DIRECTORY_#####
echo "The server will be installed in the directory of this 'setup.sh'."
echo "OK | NO"

read DIRECTORY

if ! [ $DIRECTORY == "OK" ]
then
	echo "Stop setup..."
	exit 0
fi

#####_TYPE_#####
echo "What kind of server do you want?"
echo "VANILLA | SPIGOT"

read TYPE

type $TYPE

#####_VERSION_#####
echo "What version do you want?"
echo "1.8.9 | 1.14.4 | 1.16.1 | 1.19.3"

read VERSION

version $VERSION

#####_CONFIRM_#####
echo "Are you fine with these settings:"
echo "Server: $TYPE Version: $VERSION"
echo "OK | NO"

read CONFIRM

if ! [ $CONFIRM == "OK" ]
then
        echo "Stop setup..."
        exit 0
fi

echo "Installation starts now. Please dont interrupt."

#####_JAVA_#####
java -version
if ! [ $? -eq 0 ]
then
	echo "Problem with java. Is java installed?"
	exit 1
fi

#####_SERVER.JAR_#####
URL="NULL"
if [ $TYPE == "VANILLA" ]
then
	URL="https://serverjars.com/api/fetchJar/vanilla/vanilla/$VERSION"
	echo "Download from: $URL"
fi
if [ $TYPE == "SPIGOT" ]
then
	URL="https://download.getbukkit.org/spigot/spigot-$VERSION.jar"
	echo "Download from: $URL"
fi

wget -O server.jar $URL

echo -e "#!/bin/bash\njava -Xmx2048M -Xms2048M -jar server.jar nogui" > autostart.sh
chmod +x autostart.sh
./autostart.sh

sed 's/eula=false/eula=true/' <eula.txt >eulaneu.txt
rm eula.txt
mv eulaneu.txt "eula.txt"

./autostart.sh
